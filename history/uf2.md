# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?


## mainboard model, link to manual, link to product
 
 para saber el modelo de la placa base el comando sudo dmidecode -t 2 
 nos va a servir 
 ```
 [root@a13 ~]#  dmidecode -t 2 
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
	Manufacturer: Gigabyte Technology Co., Ltd.
	Product Name: H81M-S2PV
	Version: x.x
	Serial Number: To be filled by O.E.M.
	Asset Tag: To be filled by O.E.M.
	Features:
		Board is a hosting board
		Board is replaceable
	Location In Chassis: To be filled by O.E.M.
	Chassis Handle: 0x0003
	Type: Motherboard
	Contained Object Handles: 0

 ```
 
### memory banks (free or occupied)

sudo dmidecode -t 16 este comando nos va a domostrar esta informacion 
```
[root@a13 ~]# dmidecode -t 16 
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0007, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: None
	Maximum Capacity: 32 GB
	Error Information Handle: Not Provided
	Number Of Devices: 2

```

### how many disks and types can be connected
atraves de la pagina oficial de la placa base 
```
Storage Interface
Chipset:

    2 x SATA 6Gb/s connectors (SATA3 0~SATA3 1) supporting up to 2 SATA 6Gb/s devices
    2 x SATA 3Gb/s connectors (SATA2 2~ SATA2 3) supporting up to 2 SATA 3Gb/s devices

```

### chipset, link to 

```
Intel® H81 Express Chipset
```
## cpu
atraves del comando lscpu se puede saber cual procesador lleva nuestro ordenador y mucha mas informacion
```
[root@a13 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3258 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1730.593
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6384.84
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB
NUMA node0 CPU(s):               0,1
Vulnerability Itlb multihit:     KVM: Mitigation: VMX disabled
Vulnerability L1tf:              Mitigation; PTE Inversion; VMX conditional cach
                                 e flushes, SMT disabled
Vulnerability Mds:               Mitigation; Clear CPU buffers; SMT disabled
Vulnerability Meltdown:          Mitigation; PTI
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled v
                                 ia prctl and seccomp
Vulnerability Spectre v1:        Mitigation; usercopy/swapgs barriers and __user
                                  pointer sanitization
Vulnerability Spectre v2:        Mitigation; Full generic retpoline, IBPB condit
                                 ional, IBRS_FW, STIBP disabled, RSB filling
Vulnerability Srbds:             Mitigation; Microcode
Vulnerability Tsx async abort:   Not affected
Flags:                           fpu vme de pse tsc msr pae mce cx8 apic sep mtr
                                 r pge mca cmov pat pse36 clflush dts acpi mmx f
                                 xsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rd
                                 tscp lm constant_tsc arch_perfmon pebs bts rep_
                                 good nopl xtopology nonstop_tsc cpuid aperfmper
                                 f pni pclmulqdq dtes64 monitor ds_cpl vmx est t
                                 m2 ssse3 sdbg cx16 xtpr pdcm pcid sse4_1 sse4_2
                                  movbe popcnt tsc_deadline_timer xsave rdrand l
                                 ahf_lm abm cpuid_fault invpcid_single pti ssbd 
                                 ibrs ibpb stibp tpr_shadow vnmi flexpriority ep
                                 t vpid ept_ad fsgsbase tsc_adjust erms invpcid 
                                 xsaveopt dtherm arat pln pts md_clear flush_l1d
```
### cpu model, year, cores, threads, cache 
se puede usar el mismo comando anterior (lscpu)
vermeos que en el resultado nos aparece la informacion esta 
### socket 
atraves de una foto de la placa base la podemos saber 
en nuestro caso es 1150 
```
http://es.gigabyte.com/products/upload/products/3635/31e2344f_5.jpg
```
## pci
en la respuesta de la pregunta seguiente
### number of pci slots, lanes available
 atraves de la pagina oficial de la placa base 
 ```
 Puertos de Expansión

    1 x PCI Express x16 slot, running at x16
    2 x PCI Express x1 slots
    (The PCI Express slots conform to PCI Express 2.0 standard.)

 ```
### devices connected

comando lsusb
```
[root@a13 ~]# lsusb
Bus 002 Device 002: ID 8087:8000 Intel Corp. Integrated Rate Matching Hub
Bus 002 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 001 Device 002: ID 8087:8008 Intel Corp. Integrated Rate Matching Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 003: ID 046d:c31c Logitech, Inc. Keyboard K120
Bus 003 Device 002: ID 046d:c05a Logitech, Inc. M90/M100 Optical Mouse
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

```
### network device, model, kernel module, speed

pagina oficial de la placa base 
```

    Realtek® GbE LAN chip (10/100/1000 Mbit)

```
### audio device, model, kernel module

pagina oficial de la placa base 

```
Audio

    Realtek® ALC887 codec
    High Definition Audio
    2/4/5.1/7.1-channel
    * To configure 7.1-channel audio, you have to use an HD front panel audio module and enable the multi-channel audio feature through the audio driver.

```

### vga device, model, kernel module


## hard disks

atraves del comando lsblk
```
[root@a13 ~]# lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0     1K  0 part 
├─sda5   8:5    0   100G  0 part 
├─sda6   8:6    0   100G  0 part /
└─sda7   8:7    0     5G  0 part [SWAP]

```
### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)

